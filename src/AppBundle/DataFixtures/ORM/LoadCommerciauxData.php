<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Commerciaux;

class LoadCommerciauxData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $nom = array("e.garcia", "t.gilbert", "g.flint", "d.menart", "s.bali", "o.gerard");
        $agence = array("Lyon 03", "Lyon 03", "Toulouse", "Toulouse", "Lyon 03", "Toulouse");
        for ($i = 0; $i < 6; $i++) {
            $commercial = new Commerciaux();
            $commercial->setNom($nom[$i]);
            $commercial->setAgence($agence[$i]);
            $manager->persist($commercial);
            $manager->flush();
            $this->addReference('commercial'.$i, $commercial);
        }
    }

    public function getOrder() {
        return 1;
    }

}
