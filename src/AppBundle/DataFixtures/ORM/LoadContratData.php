<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Contrat;

class LoadContratData extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        $valorisation = array("2700", "2350", "1700", "1450", "1830", "1620","2750", "3350", "7700", "2540", "3380", "2102");
        $frais = array("770", "535", "430", "501", "308", "206","507", "503", "329", "350", "470", "319");
        $date = array(
            new \DateTime('2016-11-04'),
            new \DateTime('2016-11-09'),
            new \DateTime('2016-11-17'),
            new \DateTime('2016-11-21'),
            new \DateTime('2016-11-24'),
            new \DateTime('2016-11-28'),
            new \DateTime('2016-11-14'),
            new \DateTime('2016-11-19'),
            new \DateTime('2016-11-27'),
            new \DateTime('2016-11-11'),
            new \DateTime('2016-11-14'),
            new \DateTime('2016-11-18'));
        for ($i = 0; $i < 12; $i++) {
            $contrat = new Contrat();
            $contrat->setValorisation($valorisation[$i]);
            $contrat->setFrais($frais[$i]);
            $contrat->setDate($date[$i]);
            $contrat->setCommerciaux($this->getReference('commercial'.($i>5?$i-6:$i)));
            $manager->persist($contrat);
            $manager->flush();
        }
    }

    public function getOrder() {
        return 1;
    }

}
