<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contrat
 *
 * @ORM\Table(name="contrat")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContratRepository")
 */
class Contrat {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="valorisation", type="float")
     */
    private $valorisation;

    /**
     * @var float
     *
     * @ORM\Column(name="frais", type="float")
     */
    private $frais;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="Commerciaux", inversedBy="contrat")
     */
    protected $commerciaux;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set valorisation
     *
     * @param float $valorisation
     * @return Contrat
     */
    public function setValorisation($valorisation) {
        $this->valorisation = $valorisation;

        return $this;
    }

    /**
     * Get valorisation
     *
     * @return float 
     */
    public function getValorisation() {
        return $this->valorisation;
    }

    /**
     * Set frais
     *
     * @param float $frais
     * @return Contrat
     */
    public function setFrais($frais) {
        $this->frais = $frais;

        return $this;
    }

    /**
     * Get frais
     *
     * @return float 
     */
    public function getFrais() {
        return $this->frais;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Contrat
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate() {
        return $this->date;
    }


    /**
     * Set commerciaux
     *
     * @param \AppBundle\Entity\Commerciaux $commerciaux
     * @return Contrat
     */
    public function setCommerciaux(\AppBundle\Entity\Commerciaux $commerciaux = null)
    {
        $this->commerciaux = $commerciaux;

        return $this;
    }

    /**
     * Get commerciaux
     *
     * @return \AppBundle\Entity\Commerciaux 
     */
    public function getCommerciaux()
    {
        return $this->commerciaux;
    }
}
