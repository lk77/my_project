<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Commerciaux;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Commerciaux controller.
 *
 * @Route("commerciaux")
 */
class CommerciauxController extends Controller
{
    /**
     * Lists all commerciaux entities.
     *
     * @Route("/", name="commerciaux_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $commerciauxes = $em->getRepository('AppBundle:Commerciaux')->findAll();

        return $this->render('commerciaux/index.html.twig', array(
            'commerciauxes' => $commerciauxes,
        ));
    }

    /**
     * Creates a new commerciaux entity.
     *
     * @Route("/new", name="commerciaux_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $commerciaux = new Commerciaux();
        $form = $this->createForm('AppBundle\Form\CommerciauxType', $commerciaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commerciaux);
            $em->flush($commerciaux);

            return $this->redirectToRoute('commerciaux_show', array('id' => $commerciaux->getId()));
        }

        return $this->render('commerciaux/new.html.twig', array(
            'commerciaux' => $commerciaux,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a commerciaux entity.
     *
     * @Route("/{id}", name="commerciaux_show")
     * @Method("GET")
     */
    public function showAction(Commerciaux $commerciaux)
    {
        $deleteForm = $this->createDeleteForm($commerciaux);

        return $this->render('commerciaux/show.html.twig', array(
            'commerciaux' => $commerciaux,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing commerciaux entity.
     *
     * @Route("/{id}/edit", name="commerciaux_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Commerciaux $commerciaux)
    {
        $deleteForm = $this->createDeleteForm($commerciaux);
        $editForm = $this->createForm('AppBundle\Form\CommerciauxType', $commerciaux);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('commerciaux_edit', array('id' => $commerciaux->getId()));
        }

        return $this->render('commerciaux/edit.html.twig', array(
            'commerciaux' => $commerciaux,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a commerciaux entity.
     *
     * @Route("/{id}", name="commerciaux_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Commerciaux $commerciaux)
    {
        $form = $this->createDeleteForm($commerciaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commerciaux);
            $em->flush($commerciaux);
        }

        return $this->redirectToRoute('commerciaux_index');
    }

    /**
     * Creates a form to delete a commerciaux entity.
     *
     * @param Commerciaux $commerciaux The commerciaux entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Commerciaux $commerciaux)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('commerciaux_delete', array('id' => $commerciaux->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
