<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $contrats_com = $em->getRepository('AppBundle:Contrat')->findLastRanking();
        $contrats_agency = $em->getRepository('AppBundle:Contrat')->findLastRankingByAgency();
        return $this->render('default/index.html.twig', array(
            'contrats' => $contrats_com,
            'contrats_agency' => $contrats_agency,
        ));
    }
}
